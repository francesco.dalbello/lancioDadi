﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LancioDeiDadiForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            grafico1.Visible = false;
            label8.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            try
            {
                Giocatore g = new Giocatore(Int32.Parse(textboxLanci.Text));  //prende valore da texbox e lo trasforma in un intero
                g.EseguiTutteLePartite();
                label8.Visible = true;
                label2.Visible = true;
                label3.Visible = true;
                label4.Visible = true;
                minlabel.Text = Convert.ToString(g.stat.Minimo());
                maxlabel.Text = Convert.ToString(g.stat.Massimo());
                medialabel.Text = Convert.ToString(g.stat.Media());

                grafico1.Visible = true;
                for (int i = 0; i < g.stat.Distribuzione().Length; i++)
                {
                    string freqPerc = Convert.ToString(g.stat.Distribuzione()[i] * 100.0 / g.ElencoPartite.Length);
                    grafico1.Series["Frequenza"].Points.AddXY(i, g.stat.Distribuzione()[i]);
                    //grafico1.Series["Frequenza"].Label = "#PERCENT";
                }
            }
            catch
            {
                MessageBox.Show("Inserisci il numero di lanci da fare");
            }
            
             
          


        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
