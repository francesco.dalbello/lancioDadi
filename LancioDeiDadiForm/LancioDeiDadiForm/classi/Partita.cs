﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LancioDeiDadiForm
{
    public class Partita
    {
        //PROPRIETA'
        public Dado Dado1 { get; set; }
        public Dado Dado2 { get; set; }
        public int contatore { get; set; }
        public bool finita { get; set; }

        //COSTRUTTORE (metodo speciale che inizializza)
        public Partita()
        {
            Dado1 = new Dado();
            Dado2 = new Dado();  //crea il dado prendendo il metodo "Dado" nella classe Dado.cs
            contatore = 0;       //contatore a 0 (inizia la partita)
        }

        public void EseguiTentativo()
        {
            Dado1.Lancia();  //cambia il valore di Dado1, richiamando il metodo "Lancia" in Dado.cs
            Dado2.Lancia();
            contatore++;

            if (Dado1.Valore() == Dado2.Valore())
            {
                finita = true;
            }


        }
         


    }
}