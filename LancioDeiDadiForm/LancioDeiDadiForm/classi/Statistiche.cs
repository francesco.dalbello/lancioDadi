﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LancioDeiDadiForm
{
    class Statistiche
    {
        public Partita[] ElencoPartite { get; set; }
        public Statistiche(Partita[] elenco)
        {
            ElencoPartite = elenco;
        }

        public double Media()
        {
            int i = 0;
            int s = 0;
            while (i<ElencoPartite.Length)
            {
                s += ElencoPartite[i].contatore;
                i++;
            }
            return s / ElencoPartite.Length;
        }

        public int Minimo ()
        {
            int m = ElencoPartite[0].contatore;
            for (int i = 0; i < ElencoPartite.Length; i++)
            {
                if (ElencoPartite[i].contatore < m)
                {
                    m = ElencoPartite[i].contatore;
                }

            }
            return m;
        }

        public int Massimo()
        {
            int m = ElencoPartite[0].contatore;
            for (int i = 0; i < ElencoPartite.Length; i++)
            {
                if (ElencoPartite[i].contatore > m)
                {
                    m = ElencoPartite[i].contatore;
                }

            }
            return m;
        }

        public int[] Distribuzione()
        {
            int[] d = new int[Massimo() + 1];
            for (int i = 0; i < ElencoPartite.Length; i++)
            {
                d[ElencoPartite[i].contatore]++;

            }
            return d;

        }



    }
}
