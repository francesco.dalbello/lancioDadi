﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LancioDeiDadiForm
{
    public class Dado
    {
        //CAMPI PRIVATI
        private int valore;
        private Random rnd;

        /*//PROPRIETA' PUBBLICHE
        public int Valore { get; private set; } */

        //COSTRUTTORE
        public Dado()
        {
            valore = 0;
            rnd = new Random(Guid.NewGuid().GetHashCode());
        }

        //METODI
        public void Lancia()
        {
    
            valore = rnd.Next(1, 7);
        }

        public int Valore()
        {
            return valore;
        }
    }

   
}
