﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LancioDeiDadiForm
{
    class Giocatore
    {
        public Partita[] ElencoPartite { get; set; }
        public Statistiche stat { get; set; }
        public Giocatore (int numeroPartite)   //COSTRUTTORE
        {
            ElencoPartite = new Partita[numeroPartite];   //array di partite
            stat = new Statistiche(ElencoPartite);
        }

        public void EseguiPartita(int i)
        {
            Partita p = new Partita();
            while (!p.finita)
            {
                p.EseguiTentativo();
            }
            ElencoPartite[i] = p;
        }

        public void EseguiTutteLePartite()
        {
            for (int i = 0; i < ElencoPartite.Length; i++)
            {
                EseguiPartita(i);
            }
        }

        

       
    }
}
